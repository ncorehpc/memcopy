PROGRAM ?= memcopy

override CFLAGS += $(XCFLAGS) -Xlinker --defsym=__heap_size=0xf0000 -Xlinker --defsym=__stack_size=0xf000
$(PROGRAM): $(wildcard *.c) $(wildcard *.h) $(wildcard *.S)
	echo "CFLAGS = $(CFLAGS)"
	$(CC) $(CFLAGS) $(LDFLAGS) $(filter %.c %.S,$^) $(LOADLIBES) $(LDLIBS) -o $@

clean:
	rm -f $(PROGRAM) $(PROGRAM).hex
